//////////////////
// Global State //
//////////////////
const state = {
  game: {
    level: 'difficult',
    actions: [],
  },
};

/////////
// DOM //
/////////
const $ = (selector) => document.querySelector(selector);
const handleClick = (element, handler) => {
  element.addEventListener('click', handler);
};

///////////////
// Selectors //
///////////////
const hudNode = $('.hud');
const scoreNode = $('.score-lbl');
const cubeCountNode = $('.cube-count-lbl');

// Top-level menu containers
const menuContainerNode = $('.menus');
const menuMainNode = $('.menu--main');
const menuScoreNode = $('.menu--score');

const finalScoreLblNode = $('.final-score-lbl');

///////////
// Score //
///////////
function renderScoreHud() {
  cubeCountNode.innerText = `${state.game.actions.join(',')}`;
}

function toggleScoreHud(show) {
  hudNode.style.opacity = show ? 1 : 0;
}

////////////////////
// Button Actions //
////////////////////

// Main Menu
handleClick($('.play-normal-btn'), () => {
  // display
  renderScoreHud();
  toggleScoreHud(true);

  menuMainNode.classList.remove('active');
});
handleClick($('.btn-back'), () => {
  // display
  renderScoreHud();
  toggleScoreHud(true);

  menuScoreNode.classList.remove('active');
});
handleClick($('.submit-btn'), () => {
  var request = new XMLHttpRequest();
  var data = JSON.stringify({
    level: state.game.level,
    actions: state.game.actions.join(','),
  });
  request.open('POST', '/play', true);
  request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  request.onload = function () {
    endGame(JSON.parse(request.responseText));
  };
  request.send(data);
});

//////////////////
// GAME ACTIONS //
//////////////////
function endGame(response) {
  switch (response.result) {
    case 'WON':
      finalScoreLblNode.innerText = 'Ring is destroyed!';
      break;
    case 'NOTFINISHED':
      finalScoreLblNode.innerText = 'Nothing is found!';
      break;
    case 'OUTOFRANGE':
      finalScoreLblNode.innerText = 'Falls out of the map!';
      break;
    case 'DEATH':
      finalScoreLblNode.innerText = 'Orc found, Frodo is dead!';
      break;
    default:
  }
  toggleScoreHud(false);
  state.game.actions = [];
  menuScoreNode.classList.add('active');
}

////////////////////////
// KEYBOARD SHORTCUTS //
////////////////////////

window.addEventListener('keydown', (event) => {
  switch (event.key) {
    // reset actions
    case 'r':
      state.game.actions = [];
      break;

    // navigate north
    case 'n':
    case 's':
    case 'e':
    case 'w':
      state.game.actions.push(event.key);
      break;
  }
  renderScoreHud();
});
