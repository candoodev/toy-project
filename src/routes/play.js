const store = require('../redux/store');

const actionsCreators = require('../redux/actions');
const { commands } = require('../../config');

const playRoute = (req, res) => {
  const actions = req.sanitized.actions.split(',');

  // listen to the store changes
  const unsubscribe = store.subscribe(() => {
    const state = store.getState();

    // the game has come to an end, let's reply to the user
    if (state.end) {
      res.json({
        result: state.result,
      });
      unsubscribe();
    }
  });

  // dispatch all the actions
  actions.map((action) => store.dispatch(actionsCreators[commands[action]]()));

  // verify the game has ended
  state = store.getState();
  if (!state.end) {
    store.dispatch(actionsCreators.gameEnd());
  }
};

module.exports = playRoute;
