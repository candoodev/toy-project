const config = {
	n: "navigateNorth",
	s: "navigateSouth",
	e: "navigateEast",
	w: "navigateWest",
};

module.exports = config;
