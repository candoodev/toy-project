const validateRoute = require('../validator');

const next = jest.fn();
const json = jest.fn();
const res = {
  status: () => ({
    json,
  }),
};

describe('Validate route', () => {
  beforeEach(() => {
    json.mockReset();
  });
  describe('If we call the route without valid params', () => {
    it('should fail', () => {
      const req = {
        body: {
          actions: 'xxxx',
        },
      };
      validateRoute(req, res, next);
      expect(next).not.toHaveBeenCalled();
      expect(json).toHaveBeenCalled();
    });
  });
  describe('If we call the route wit valid params', () => {
    it('should pass', () => {
      const req = {
        body: {
          actions: 'n,s,w',
        },
      };
      validateRoute(req, res, next);
      expect(next).toHaveBeenCalled();
      expect(json).not.toHaveBeenCalled();
    });
  });
});
