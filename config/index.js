const commands = require("./commands");
const levels = require("./levels");

const config = {
	commands,
	levels,
};

module.exports = config;
