const NAVIGATE_NORTH = '@APP/NAVIGATE_NORTH';
const NAVIGATE_SOUTH = '@APP/NAVIGATE_SOUTH';
const NAVIGATE_EAST = '@APP/NAVIGATE_EAST';
const NAVIGATE_WEST = '@APP/NAVIGATE_WEST';
const NAVIGATE_RESET = '@APP/NAVIGATE_RESET';

const GAME_LOAD_LEVEL = '@APP/GAME_LOAD_LEVEL';
const GAME_END = '@APP/GAME_END';
const GAME_SET_STATE = '@APP/GAME_SET_STATE';

const navigateNorth = () => ({
  type: NAVIGATE_NORTH,
});
const navigateSouth = () => ({
  type: NAVIGATE_SOUTH,
});
const navigateEast = () => ({
  type: NAVIGATE_EAST,
});
const navigateWest = () => ({
  type: NAVIGATE_WEST,
});
const navigateReset = () => ({
  type: NAVIGATE_RESET,
});

const loadLevel = (payload) => ({
  type: GAME_LOAD_LEVEL,
  payload,
});

const gameEnd = (payload) => ({
  type: GAME_END,
  payload,
});

const gameSetState = (payload) => ({
  type: GAME_SET_STATE,
  payload,
});

module.exports = {
  NAVIGATE_NORTH,
  NAVIGATE_SOUTH,
  NAVIGATE_EAST,
  NAVIGATE_WEST,
  NAVIGATE_RESET,
  GAME_LOAD_LEVEL,
  GAME_END,
  GAME_SET_STATE,
  loadLevel,
  navigateNorth,
  navigateSouth,
  navigateEast,
  navigateWest,
  navigateReset,
  gameEnd,
  gameSetState,
};
