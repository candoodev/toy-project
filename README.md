# Destroy Da One Ring - Game

Thanks for giving me the chance to complete this game.
It was fun doing it and of course there could be plenty of things to improve ;)

I try to summarize what i did in a few short sentences below.

### General

For rapid prototyping i use as little tools and noise as possible.
This reduces a lot of overhead and you can accomplish faster results in a shorter period of time.

#### Testing

I just added two tests to showcase how a setup could look like, of course more would be required, especially for the reducers and the core functionality! ;)

### Run the project

The project can be run by following those steps:

`yarn` // install all packages

`npm start` // start the application

`open http://localhost:3000` // open the browser on port 3000 to start the game

### Development

For development mode you need to run `npm run debug` instead of `npm start`.

As well by running `npm run test:watch` you can enter watch mode of the available tests.

## Server:

I solving the example by using redux as the state management, so that features could be easily applied like logging time traveling etc.

The redux setup could be enhanced even further but due to the code size i avoided over engineering things.

Something which i find nice is that the middleware can easily be replaced by a differend game logic.

Code quality tools like linter, prettier etc. would be good to add but i outscoped it as part of the task.

## Client:

Client side would require quite a few adjustments...
Here the approach was really to get it up and running.
Therefore i just coded vanilla css and javascript.

Hope you enjoy it!
