const {
  NAVIGATE_NORTH,
  NAVIGATE_SOUTH,
  NAVIGATE_EAST,
  NAVIGATE_WEST,
  NAVIGATE_RESET,
  GAME_LOAD_LEVEL,
  GAME_SET_STATE,
} = require('./actions');

const initialState = {
  x: 0,
  y: 0,
  game: [],
  current: null,
  result: null,
  end: false,
};

const findStartingPoint = (game) => {
  let x = 0;
  let y = 0;
  game.map((yContent, indexY) => {
    yContent.map((xContent, indexX) => {
      if (xContent === 'F') {
        x = indexX;
        y = indexY;
      }
    });
  });

  return { x, y, game };
};

const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case GAME_SET_STATE:
      return {
        ...state,
        ...action.payload,
      };
    // navigate north
    case NAVIGATE_NORTH:
      return {
        ...state,
        y: state.y - 1,
      };
    // navigate south
    case NAVIGATE_SOUTH:
      return {
        ...state,
        y: state.y + 1,
      };
    // navigate east
    case NAVIGATE_EAST:
      return {
        ...state,
        x: state.x + 1,
      };
    // navigate west
    case NAVIGATE_WEST:
      return {
        ...state,
        x: state.x - 1,
      };
    // reset navigator (testing)
    case NAVIGATE_RESET:
      return {
        ...state,
        ...initialState,
      };
      break;
    // load game level
    case GAME_LOAD_LEVEL:
      return {
        ...state,
        ...initialState,
        ...findStartingPoint(action.payload),
      };
    default:
      return {
        ...state,
      };
  }
};

module.exports = mainReducer;
