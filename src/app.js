const express = require('express');
const bodyParser = require('body-parser');

const app = express();

const { level, play, validator } = require('./routes/');

const serverPort = 3000;
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

app.get(['/health', '/robots.txt', '/favicon.ico'], (req, res) => {
  res.status(200).send('Ok');
});

app.post('/play', validator, level, play);

app.listen(serverPort, () => {
  console.info(
    `==> 💻  Open http://localhost:${serverPort} in a browser to view the app.`
  );
});
