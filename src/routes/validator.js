const Joi = require('@hapi/joi');
const HttpStatus = require('http-status-codes');

const { levels, commands } = require('../../config');

const schemaArray = Joi.array().items(
  Joi.string().valid(...Object.keys(commands))
);

const schema = Joi.object({
  level: Joi.string()
    .valid(...Object.keys(levels))
    .default(Object.keys(levels)[0]),
  actions: Joi.string()
    .required()
    .custom((value, helper) => {
      const { error } = schemaArray.validate(value.split(','));
      if (!error) {
        return value;
      } else {
        throw error;
      }
    }),
});

const validatorRoute = (req, res, next) => {
  const { error, value } = schema.validate(req.body);

  // create our custom property with all the required data
  req.sanitized = {
    ...value,
  };

  if (error) {
    res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
      message: error.details.map((err) => err.message),
    });
  } else {
    next();
  }
};

module.exports = validatorRoute;
