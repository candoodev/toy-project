const level = require("./level");
const play = require("./play");
const validator = require("./validator");

module.exports = {
	level,
	play,
	validator,
};
