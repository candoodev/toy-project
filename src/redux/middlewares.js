const { applyMiddleware } = require('redux');

const {
  NAVIGATE_NORTH,
  NAVIGATE_SOUTH,
  NAVIGATE_EAST,
  NAVIGATE_WEST,
  GAME_END,
  gameSetState,
} = require('./actions');

const gameMiddleware = (store) => (next) => (action) => {
  // perform incoming action
  const res = next(action);

  if (
    action.type === NAVIGATE_NORTH ||
    action.type === NAVIGATE_SOUTH ||
    action.type === NAVIGATE_EAST ||
    action.type === NAVIGATE_WEST
  ) {
    // get the new state
    const state = store.getState();
    const newState = {};

    // try to get the new value
    try {
      newState.current = state.game[state.y][state.x];
    } catch (e) {
      newState.current = 'X';
    }

    switch (newState.current) {
      // O is an Orc. If Frodo encounters it, it dies.
      case 'O':
        newState.result = 'DEATH';
        newState.end = true;
        break;

      // D is Mount Doom, where Frodo needs to get to destroy the ring.
      case 'D':
        newState.result = 'WON';
        newState.end = true;
        break;

      // Proceed
      case 'F':
      case '-':
        break;

      // Out of the map or unknown
      default:
        newState.result = 'OUTOFRANGE';
        newState.end = true;
    }
    // let's update the new state
    next(gameSetState(newState));
  }

  if (action.type === GAME_END) {
    // let's update the new state
    next(
      gameSetState({
        result: 'NOTFINISHED',
        end: true,
      })
    );
  }
  return res;
};

module.exports = applyMiddleware(gameMiddleware);

// "actions": "n,n,n,e,e,e,n,e,n,e,e,e,s"
