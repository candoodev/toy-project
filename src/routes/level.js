const store = require('../redux/store');
const config = require('../../config');
const { loadLevel } = require('../redux/actions');

const levelRoute = (req, res, next) => {
  store.dispatch(loadLevel(config.levels[req.sanitized.level]));
  next();
};

module.exports = levelRoute;
