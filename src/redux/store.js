const { createStore } = require("redux");

const reducers = require("./reducers");
const middlewares = require("./middlewares");

const store = createStore(reducers, middlewares);

module.exports = store;
